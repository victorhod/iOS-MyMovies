//
//  MenuLateralEsquerdoViewCell.h
//  Santa Rosa
//
//  Created by Etica on 20/07/16.
//  Copyright © 2016 Luis Teodoro. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MenuLateralEsquerdoViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgIconeMenu;
@property (weak, nonatomic) IBOutlet UILabel *txtItensMenu;

@end
