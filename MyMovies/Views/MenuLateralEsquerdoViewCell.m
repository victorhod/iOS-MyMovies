//
//  MenuLateralEsquerdoViewCell.m
//  Santa Rosa
//
//  Created by Etica on 20/07/16.
//  Copyright © 2016 Luis Teodoro. All rights reserved.
//

#import "MenuLateralEsquerdoViewCell.h"

@implementation MenuLateralEsquerdoViewCell
@synthesize txtItensMenu, imgIconeMenu;

- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

@end
