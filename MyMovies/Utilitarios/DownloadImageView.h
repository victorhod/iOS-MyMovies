//
//  DownloadImageView.h
//  Carros
//
//  Created by Etica on 11/07/16.
//  Copyright © 2016 FVRR. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DownloadImageView : UIImageView{
    NSString *url;
    UIActivityIndicatorView *progress;
    NSOperationQueue *queue;
}

@property (nonatomic, copy)NSString *url;

@end
