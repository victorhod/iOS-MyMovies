//
//  Alerta.m
//  Carros
//
//  Created by Etica on 08/07/16.
//  Copyright © 2016 FVRR. All rights reserved.
//

#import "Funcionalidades.h"



@implementation Funcionalidades


+(void)alerta:(NSString *)msg{
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alerta" message:msg delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
    
    [alert show];
}
@end
