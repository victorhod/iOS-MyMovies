//
//  InformacoesViewController.h
//  MyMovies
//
//  Created by Etica on 30/09/16.
//  Copyright © 2016 Victor Freitas. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Filme.h"
#import "AppDelegate.h"
#import "DownloadImageView.h"

@interface InformacoesViewController : UIViewController{
    
    Filme *filme;
}

@property (nonatomic, retain) Filme *filme;
@property (weak, nonatomic) IBOutlet DownloadImageView *imagemFilme;
@property (weak, nonatomic) IBOutlet UILabel *tituloFilme;
@property (weak, nonatomic) IBOutlet UILabel *anoFilme;
@property (weak, nonatomic) IBOutlet UILabel *classificacaoFilme;
@property (weak, nonatomic) IBOutlet UILabel *lancadoFilme;
@property (weak, nonatomic) IBOutlet UILabel *generoFilme;
@property (weak, nonatomic) IBOutlet UITextView *sinapseFilme;
@property (weak, nonatomic) IBOutlet UILabel *direcaoFilme;
@property (weak, nonatomic) IBOutlet UILabel *escritoresFilme;
@property (weak, nonatomic) IBOutlet UILabel *atoresFilme;
@property (weak, nonatomic) IBOutlet UILabel *linguagemFilme;
@property (weak, nonatomic) IBOutlet UILabel *estadoFilme;
@property (weak, nonatomic) IBOutlet UILabel *premiosFilme;
@property (weak, nonatomic) IBOutlet UILabel *pontosFilme;
@property (weak, nonatomic) IBOutlet UILabel *classificacaoIMDBFime;
@property (weak, nonatomic) IBOutlet UILabel *votosIMDBFilme;

@property (weak, nonatomic) IBOutlet UIButton *btAdicionaFavoritos;
@property (weak, nonatomic) IBOutlet UIButton *btRemoveFavoritos;

@property (weak, nonatomic) IBOutlet UIScrollView *scrollview;

- (IBAction)btAdicionarFavoritos:(id)sender;
- (IBAction)btRemoverFavoritos:(id)sender;

@end
