//
//  FavoritosViewController.h
//  MyMovies
//
//  Created by Etica on 30/09/16.
//  Copyright © 2016 Victor Freitas. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "DownloadImageView.h"
#import "Filme.h"
#import "InformacoesViewController.h"

@interface FavoritosViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>{
    
    
}
@property (weak, nonatomic) IBOutlet DownloadImageView *imagemCelula;
@property (weak, nonatomic) IBOutlet UILabel *tituloCelula;
@property (weak, nonatomic) IBOutlet UILabel *sinapseCelula;
@property (weak, nonatomic) IBOutlet UITableView *tabelaFavoritos;
@property (weak, nonatomic) IBOutlet UILabel *lbNenhumFavorito;

@end
