//
//  ProcurarViewController.h
//  MyMovies
//
//  Created by Etica on 30/09/16.
//  Copyright © 2016 Victor Freitas. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SystemConfiguration/SystemConfiguration.h>
#import "Filme.h"
#import "HttpAsyncFilmes.h"
#import "SKDropDown.h"
#import "PostFilme.h"
#import "Funcionalidades.h"
#import "SBJsonParser.h"
#import "DownloadImageView.h"
#import "InformacoesViewController.h"

@interface ProcurarViewController : UIViewController<UITableViewDelegate, UITableViewDataSource,HttpAsyncHelperDelegate, SKDropDownDelegate, UITextFieldDelegate>{
    
    NSMutableArray *Filmes;
}

@property (nonatomic, retain) IBOutlet UIActivityIndicatorView *progress;
@property (strong, nonatomic) SKDropDown *dropDown;

@property (weak, nonatomic) IBOutlet UITextField *txtTituloFilme;
@property (weak, nonatomic) IBOutlet UITextField *txtAnoFilme;
@property (weak, nonatomic) IBOutlet UIButton *btSinapse;
@property (weak, nonatomic) IBOutlet UITableView *tabelaFilmes;
@property (weak, nonatomic) IBOutlet UIImageView *imagemDropDown;
@property (weak, nonatomic) IBOutlet UIButton *btPesquisar;

- (IBAction)btPesquisar:(id)sender;
- (IBAction)btSinapse:(id)sender;

-(NSMutableArray *)parserJSON:(NSData *)data;

@end
