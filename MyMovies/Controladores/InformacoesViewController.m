//
//  InformacoesViewController.m
//  MyMovies
//
//  Created by Etica on 30/09/16.
//  Copyright © 2016 Victor Freitas. All rights reserved.
//

#import "InformacoesViewController.h"

@interface InformacoesViewController (){
    
    NSManagedObjectContext *context;

}

@end

@implementation InformacoesViewController
@synthesize filme;
@synthesize title, anoFilme, classificacaoFilme, lancadoFilme, generoFilme, sinapseFilme, imagemFilme, btRemoveFavoritos, btAdicionaFavoritos, scrollview;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.scrollview setScrollEnabled:YES];
    [self.scrollview setContentSize: (CGSizeMake(320, 1000))];

    AppDelegate *delegate = [[UIApplication sharedApplication]delegate];
    context = [delegate managedObjectContext];
    
    [sinapseFilme setUserInteractionEnabled:NO];
    
    self.navigationItem.title = self.tituloFilme.text = filme.titulo;
;
    if(filme.url_foto == nil){
        
        self.imagemFilme.image = [UIImage imageNamed: @"No_image.png"];
        
    }else{
        
        self.imagemFilme.url = filme.url_foto;
    }

    self.anoFilme.text = filme.ano;
    self.classificacaoFilme.text = filme.classificacao;
    self.lancadoFilme.text = filme.lancamento;
    self.generoFilme.text = filme.genero;
    self.sinapseFilme.text = filme.sinapse;
    self.direcaoFilme.text = [filme.diretores description];
    self.escritoresFilme.text = [filme.escritores description];
    self.atoresFilme.text = [filme.atores description];
    self.linguagemFilme.text = [filme.linguagens description];
    self.estadoFilme.text = filme.pais;
    self.premiosFilme.text = [filme.premios description];
    self.pontosFilme.text = filme.pontuacao;
    self.classificacaoIMDBFime.text = filme.classificacaoIMDB;
    self.votosIMDBFilme.text = filme.votosIMDB;
    
    [self verificaFavoritos];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


/**
 *(Português) Ação 'btAdicionarFavoritos', Adiciona um favorito ao banco de dados interno.
 *
 *(English) Action 'btAdicionarFavoritos' Adds a favorite to the internal database.
 * @author Victor Freitas (victor.alves@eticasolucoes.com.br).
 * @param "id".
 */
- (IBAction)btAdicionarFavoritos:(id)sender {
    
    NSEntityDescription *tabela = [NSEntityDescription entityForName:@"Favoritos" inManagedObjectContext:context];
    NSManagedObject *novoFilme= [[NSManagedObject alloc] initWithEntity:tabela insertIntoManagedObjectContext:context];
    
    [novoFilme setValue:self.filme.titulo forKey:@"titulo"];
    [novoFilme setValue:self.anoFilme.text forKey:@"ano"];
    [novoFilme setValue:self.imagemFilme.url forKey:@"url_imagem"];
    [novoFilme setValue:self.classificacaoFilme.text forKey:@"classificacao"];
    [novoFilme setValue:self.lancadoFilme.text forKey:@"lancamento"];
    [novoFilme setValue:self.generoFilme.text forKey:@"genero"];
    [novoFilme setValue:self.sinapseFilme.text forKey:@"sinapse"];
    [novoFilme setValue:self.direcaoFilme.text forKey:@"direcao"];
    [novoFilme setValue:self.escritoresFilme.text forKey:@"escrito"];
    [novoFilme setValue:self.atoresFilme.text forKey:@"atores"];
    [novoFilme setValue:self.linguagemFilme.text forKey:@"linguagens"];
    [novoFilme setValue:self.estadoFilme.text forKey:@"estado"];
    [novoFilme setValue:self.premiosFilme.text forKey:@"premios"];
    [novoFilme setValue:self.pontosFilme.text forKey:@"pontos"];
    [novoFilme setValue:self.classificacaoIMDBFime.text forKey:@"classificacaoIMDB"];
    [novoFilme setValue:self.votosIMDBFilme.text forKey:@"votosIMDB"];
    [novoFilme setValue:self.filme.imdbID forKey:@"id"];
    
    NSError *error;
    [context save:&error];
    
    NSLog(@"Filme Adicionado !");
    
    [btAdicionaFavoritos setHidden:YES];
    [btRemoveFavoritos setHidden:NO];

}

/**
 *(Português) Ação 'btRemoverFavoritos',Remove um favorito do banco de dados interno.
 *
 *(English) Action 'btRemoverFavoritos' Remove hum favorite Bank internal data.
 * @author Victor Freitas (victor.alves@eticasolucoes.com.br).
 * @param "id".
 */
- (IBAction)btRemoverFavoritos:(id)sender {
    
    NSEntityDescription *tabela = [NSEntityDescription entityForName:@"Favoritos" inManagedObjectContext:context];
    
    NSFetchRequest *request = [[NSFetchRequest alloc]init];
    [request setEntity:tabela];
    
    NSPredicate *predicado = [NSPredicate 	predicateWithFormat:@"id like %@ ", self.filme.imdbID];
    
    [request setPredicate:predicado];
    
    NSError *error;
    NSArray *matchingData = [context executeFetchRequest:request error:&error];
    
    if(matchingData.count <= 0){
        
        NSLog(@"Filme Não Deletado!");
    }else{
        
        for (NSManagedObject *obj in matchingData) {
            [context deleteObject:obj];
        }
        
        [btAdicionaFavoritos setHidden:NO];
        [btRemoveFavoritos setHidden:YES];
        NSLog(@"Filme Deletado!");
        
    }

}

/**
 *(Português) Ação 'verificaFavoritos', verifica pelo idIMDB quais os favoritos existentes no banco de dados
 * interno.
 *
 *(English) Action 'verificaFavoritos' checks by idIMDB which existing favorite in the database
 * Built.
 * @author Victor Freitas (victor.alves@eticasolucoes.com.br).
 * @param void.
 */
-(void)verificaFavoritos{
    
    
    NSEntityDescription *tabela = [NSEntityDescription entityForName:@"Favoritos" inManagedObjectContext:context];
    
    NSFetchRequest *request = [[NSFetchRequest alloc]init];
    [request setEntity:tabela];
    
    NSPredicate *predicado = [NSPredicate 	predicateWithFormat:@"id like %@", self.filme.imdbID];
    
    [request setPredicate:predicado];
    
    NSError *error;
    NSArray *matchingData = [context executeFetchRequest:request error:&error];
    
    if(matchingData.count <= 0){
        
        NSLog(@"Filme Não Econtrado !");
        
        [btAdicionaFavoritos setHidden:NO];
        [btRemoveFavoritos setHidden:YES];
        
    }else{
        
        NSLog(@"Filme Encontrado !");
        
        [btAdicionaFavoritos setHidden:YES];
        [btRemoveFavoritos setHidden:NO];
        
    }

}
@end
