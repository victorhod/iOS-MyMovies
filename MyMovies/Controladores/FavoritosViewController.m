//
//  FavoritosViewController.m
//  MyMovies
//
//  Created by Etica on 30/09/16.
//  Copyright © 2016 Victor Freitas. All rights reserved.
//

#import "FavoritosViewController.h"

@interface FavoritosViewController (){
    
    NSManagedObjectContext *context;

}

@end


NSArray *fetchedObjects;
NSMutableArray *objetos;
UIBarButtonItem *editTableButton;

@implementation FavoritosViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title = @"Favoritos";

    [_lbNenhumFavorito setHidden:YES];

    editTableButton = [[UIBarButtonItem alloc]
                                   initWithTitle:@"Editar"
                                   style:UIBarButtonItemStyleDone
                                   target:self
                                   action:@selector(editTable:)];
    
    self.navigationItem.rightBarButtonItem = editTableButton;
    

    
    AppDelegate *delegate = [[UIApplication sharedApplication]delegate];
    context = [delegate managedObjectContext];
    
    objetos = [[NSMutableArray alloc]init];
    fetchedObjects = [[NSArray alloc]init];
    
    
    [self buscarFavoritos];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
    
}

-(IBAction)editTable:(id)sender{
    
    [_tabelaFavoritos setEditing:![[self tabelaFavoritos] isEditing] animated:YES];
}

/**
 *(Português) Metodo 'buscarFavoritos' , para verificar se existe algum favorito no banco de dados interno,
 * e adicionar em um NSMutableArray para ser populado na UITableView.
 *
 *(English) Method 'buscarFavoritos, to check if there is a favorite in the internal database,
 * And add in a NSMutableArray to be populated in UITableView.
 * @author Victor Freitas (victor.alves@eticasolucoes.com.br).
 * @param void.
 */
-(void)buscarFavoritos{
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription
                                   entityForName:@"Favoritos" inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    
    NSError *error = nil;
    
    fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    
    for (NSManagedObject *info in fetchedObjects) {
        
        Filme *filme = [[Filme alloc]init];
        
        filme.titulo = [info valueForKey:@"titulo"];
        filme.sinapse = [info valueForKey:@"sinapse"];
        filme.url_foto = [info valueForKey:@"url_imagem"];
        filme.ano = [info valueForKey:@"ano"];
        filme.classificacao = [info valueForKey:@"classificacao"];
        filme.lancamento = [info valueForKey:@"lancamento"];
        filme.genero = [info valueForKey:@"genero"];
        filme.diretores = [info valueForKey:@"direcao"];
        filme.escritores = [info valueForKey:@"escrito"];
        filme.atores = [info valueForKey:@"atores"];
        filme.linguagens = [info valueForKey:@"linguagens"];
        filme.pais = [info valueForKey:@"estado"];
        filme.premios = [info valueForKey:@"premios"];
        filme.pontuacao = [info valueForKey:@"pontos"];
        filme.classificacaoIMDB = [info valueForKey:@"classificacaoIMDB"];
        filme.votosIMDB = [info valueForKey:@"votosIMDB"];
        filme.imdbID = [info valueForKey:@"id"];
        

        [objetos addObject:filme];
    }
    
    if([objetos count] == 0){
            [_tabelaFavoritos setHidden:YES];
            [_lbNenhumFavorito setHidden:NO];
    }
    
    
    
    if (!fetchedObjects) {
        
        NSLog(@"Nenhum Filme Encontrado !");
        
        [_lbNenhumFavorito setHidden:NO];
        [_tabelaFavoritos setHidden:YES];
        
        NSLog(@"Error fetching Employee objects: %@\n%@", [error localizedDescription], [error userInfo]);
        abort();
    }
    
}

#pragma mark - TableView

/**
 *(Português) Metodo 'numberOfSectionsInTableView', Retorna quantidade de sessões para criar na tabela.
 *
 *(English) Method 'numberOfSectionsInTableView' Returns number of sessions to create the table.
 * @author Victor Freitas (victor.alves@eticasolucoes.com.br).
 * @param Object "UITableView".
 */
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 1;
}

/**
 *(Português) Metodo 'numberOfRowsInSection', Retorna quantidade de linhas para criar na tabela, que é a
 * quantidade de Filmes.
 *
 *(English) Method 'numberOfRowsInSection' Returns number of lines to create the table, which is
 * Amount of movies.
 * @author Victor Freitas (victor.alves@eticasolucoes.com.br).
 * @param Object "UITableView", "NSInteger".
 */
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [objetos count];
}


/**
 *(Português) Metodo 'cellForRowAtIndexPath', Retorna a célula que vai ser o conteúdo para a linha solicitada.
 *
 *(English) Method 'cellForRowAtIndexPath', Returns the cell that will be the content for the requested line.
 * @author Victor Freitas (victor.alves@eticasolucoes.com.br).
 * @param Object "UITableView", "NSIndexPath".
 */
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = (UITableViewCell *)[self.tabelaFavoritos dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if(cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];    }

    //Recupera indice da linha selecionada.
    NSInteger linha = indexPath.row;

    Filme *filme;
    if([objetos count] != 0){
        filme = [objetos objectAtIndex:linha];
        [editTableButton setEnabled:YES];
    
 

        DownloadImageView *recipeImageView = (DownloadImageView *)[cell viewWithTag:1];
        recipeImageView.url = filme.url_foto;
        
        UILabel *recipeNameLabel = (UILabel *)[cell viewWithTag:2];
        recipeNameLabel.text = filme.titulo;
        
        UILabel *recipeDetailLabel = (UILabel *)[cell viewWithTag:3];
        recipeDetailLabel.text = filme.sinapse;
        
    }else{
        
        DownloadImageView *recipeImageView = (DownloadImageView *)[cell viewWithTag:1];
        recipeImageView.image = nil;
        
        UILabel *recipeNameLabel = (UILabel *)[cell viewWithTag:2];
        recipeNameLabel.text = nil;
        
        UILabel *recipeDetailLabel = (UILabel *)[cell viewWithTag:3];
        recipeDetailLabel.text = nil;

        tableView.dataSource = nil;
        [cell.contentView clearsContextBeforeDrawing];
        
        if([objetos count] == 0){
            [_lbNenhumFavorito setHidden:NO];
            [_tabelaFavoritos setHidden:YES];
        }
        
    }
    
    return cell;
}


/**
 *(Português) Metodo 'didSelectRowAtIndexPath', Pega a célula selecionada da Tabela.
 * solicitada.
 *
 *(English) Method 'didSelectRowAtIndexPath', Grab the selected cell of the table.
 * @author Victor Freitas (victor.alves@eticasolucoes.com.br).
 * @param Object "UITableView", "NSIndexPath".
 */
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    //Recupera indice da linha selecionada.
    NSInteger linha = indexPath.row;
    
    Filme *filme = [objetos objectAtIndex:linha];
    
    //Navega para a tela de detalhes.
    InformacoesViewController *detalhes = [[InformacoesViewController alloc]init];
    
    NSString *nomeStoryboard = @"Main";
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:nomeStoryboard bundle: nil];
    detalhes = [storyboard instantiateViewControllerWithIdentifier:@"InformacoesViewController"];
    
    
    detalhes.filme = filme;
    
    [self.navigationController pushViewController:detalhes animated:YES];
    
}

/**
 *(Português) Metodo 'canEditRowAtIndexPath', Retorna um booleano permitindo editarmos as células da tabela.
 * solicitada.
 *
 *(English) Method 'canEditRowAtIndexPath', Returns a boolean allowing we edit the table cells.
 * @author Victor Freitas (victor.alves@eticasolucoes.com.br).
 * @param Object "UITableView", "NSIndexPath".
 */
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}


/**
 *(Português) Metodo 'canEditRowAtIndexPath', No momento da edição identificamos através de métodos nativos
 * qual ação iremos tomar com a célula selecionada.
 *
 *(English) Method 'canEditRowAtIndexPath', At the time of issue identified through native methods what action
 * we will take to the selected cell
 * @author Victor Freitas (victor.alves@eticasolucoes.com.br).
 * @param Object "UITableView", "NSIndexPath".
 */
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete)
    {
        NSInteger linha = indexPath.row;
        
        Filme *filme = [objetos objectAtIndex:linha];
        
        [self removeFavorito:filme];
        [objetos removeObjectAtIndex:linha];
        
        
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
        
        if([objetos count] == 0){
            [_lbNenhumFavorito setHidden:NO];
            [_tabelaFavoritos setHidden:YES];
            [editTableButton setEnabled:NO];
            
        }
    }
}

/**
 *(Português) Metodo 'removeFavorito', para remover um filme favorito do banco de dados interno.
 *
 *(English) Method 'removeFavorito' to remove a favorite movie of the internal database. 
 * @author Victor Freitas (victor.alves@eticasolucoes.com.br).
 * @param Object "Filme".
 */
-(void)removeFavorito:(Filme *)filme{
    
    NSEntityDescription *tabela = [NSEntityDescription entityForName:@"Favoritos" inManagedObjectContext:context];
    
    NSFetchRequest *request = [[NSFetchRequest alloc]init];
    [request setEntity:tabela];
    
    NSPredicate *predicado = [NSPredicate 	predicateWithFormat:@"id like %@ ", filme.imdbID];
    
    [request setPredicate:predicado];
    
    NSError *error;
    NSArray *matchingData = [context executeFetchRequest:request error:&error];
    
    if(matchingData.count <= 0){
        
        NSLog(@"Filme Não Deletado!");
    }else{
        
        for (NSManagedObject *obj in matchingData) {
            [context deleteObject:obj];
        }
        
        NSLog(@"Filme Deletado!");
        
    }
    
}

@end
