//
//  ProcurarViewController.m
//  MyMovies
//
//  Created by Etica on 30/09/16.
//  Copyright © 2016 Victor Freitas. All rights reserved.
//

#import "ProcurarViewController.h"

#define URL_PROCURAR @"http://www.omdbapi.com/?";

@interface ProcurarViewController ()

@end

@implementation ProcurarViewController
@synthesize txtTituloFilme, txtAnoFilme, btSinapse, tabelaFilmes, progress;

PostFilme *postFilme;

/**
 *(Português) Método 'slideNavigationControllerShouldDisplayLeftMenu', Este metodo ativa o menu lateral
 * esquerdo nesta view.
 *(English) Method 'slideNavigationControllerShouldDisplayLeftMenu' This method enables the side menu
 * Left in this view.
 * @author Victor Freitas (victor.alves@eticasolucoes.com.br).
 * @param void.
 * @return BOOL
 */
- (BOOL)slideNavigationControllerShouldDisplayLeftMenu
{
    return YES;
}


/**
 * Metodo que inicializa toque na tela.
 * @author Victor Freitas
 */
-(void) inicializaGesto {
    UIGestureRecognizer *gesto  = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(clickForaTeclado:)];
    gesto.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:gesto];
}

/**
 *  Se tocar em qualquer 'ponto' fora do teclado esconde o teclado.
 * @author Victor Freitas
 */
-(IBAction)clickForaTeclado :(id)sender {
    [self.view endEditing:YES];
    
    
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self inicializaGesto];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}

#pragma mark - TableView


/**
 *(Português) Metodo 'numberOfRowsInSection', Retorna quantidade de linhas para criar na tabela, que é a
 * quantidade de Filmes.
 *
 *(English) Method 'numberOfRowsInSection' Returns number of lines to create the table, which is
 * Amount of movies.
 * @author Victor Freitas (victor.alves@eticasolucoes.com.br).
 * @param Object "UITableView", "NSInteger".
 */
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [Filmes count];
}

/**
 *(Português) Metodo 'cellForRowAtIndexPath', Retorna a célula que vai ser o conteúdo para a linha solicitada.
 *
 *(English) Method 'cellForRowAtIndexPath', Returns the cell that will be the content for the requested line.
 * @author Victor Freitas (victor.alves@eticasolucoes.com.br).
 * @param Object "UITableView", "NSIndexPath".
 */
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = (UITableViewCell *)[self.tabelaFilmes dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if(cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];    }
    
    //Filme Selecionado
    NSInteger linha = indexPath.row;
    
    Filme *filme = [Filmes objectAtIndex:linha];
    
    if(filme.url_foto == nil){
        
        DownloadImageView *recipeImageView = (DownloadImageView *)[cell viewWithTag:1];
        recipeImageView.image = [UIImage imageNamed: @"No_image.png"];
        
    }else{
        
        DownloadImageView *recipeImageView = (DownloadImageView *)[cell viewWithTag:1];
        recipeImageView.url = filme.url_foto;
        
    }
    
    UILabel *recipeNameLabel = (UILabel *)[cell viewWithTag:2];
    recipeNameLabel.text = filme.titulo;
    
    UILabel *recipeDetailLabel = (UILabel *)[cell viewWithTag:3];
    recipeDetailLabel.text = filme.sinapse;
    
    return cell;
}


/**
 *(Português) Metodo 'didSelectRowAtIndexPath', Pega a célula selecionada da Tabela.
 * solicitada.
 *
 *(English) Method 'didSelectRowAtIndexPath', Grab the selected cell of the table.
 * @author Victor Freitas (victor.alves@eticasolucoes.com.br).
 * @param Object "UITableView", "NSIndexPath".
 */
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    //Recupera indice da linha selecionada.
    NSInteger linha = indexPath.row;
    
    Filme *filme = [Filmes objectAtIndex:linha];
    
    //NAvega para a tela de detalhes.
    InformacoesViewController *detalhes = [[InformacoesViewController alloc]init];
    
    NSString *nomeStoryboard = @"Main";
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:nomeStoryboard bundle: nil];
    detalhes = [storyboard instantiateViewControllerWithIdentifier:@"InformacoesViewController"];
    
    
    detalhes.filme = filme;
    
    [self.navigationController pushViewController:detalhes animated:YES];
    
}

/**
 *(Português) Metodo 'atualizar', Inicializa a requisição para a busca do filme desejado.
 *
 *(English) Method 'didSelectRowAtIndexPath', Initializes the request for the search of the desired movie.
 * @author Victor Freitas (victor.alves@eticasolucoes.com.br).
 * @param void.
 * @return void.
 */
-(void)atualizar{
    
    //Iniciar Animação.
    [progress startAnimating];
    
    //Cria a URL Correta.
    NSString *url = URL_PROCURAR;
    
    //Cria a Classe HttpAsyncHelper
    HttpAsyncFilmes *http = [[HttpAsyncFilmes alloc] init];
    
    //informa o delegate para receber o resultado (self é a própria classe).
    [http setDelegate:self];
    
    //Dispara a requisição.
    [http doGet:url obj:postFilme];
    
}

/**
 *(Português) Ação 'btPesquisar', Dispara a requisição para a pesquisa do filme desejado.
 *
 *(English) Action 'btPesquisar', Triggers the request for the search of the desired movie.
 * @author Victor Freitas (victor.alves@eticasolucoes.com.br).
 * @param id.
 * @return IBAction.
 */
- (IBAction)btPesquisar:(id)sender {
    
    if([self validaCamposObrigatorios]){
        
        if([self currentNetworkStatus]){
            
            [_btPesquisar setEnabled:NO];
            
            postFilme = [[PostFilme alloc]init];
            
            postFilme.titulo = txtTituloFilme.text;
            postFilme.ano = txtAnoFilme.text;
            postFilme.sinapse = btSinapse.titleLabel.text;
            
            [self atualizar];
            
        }else{
            
            [Funcionalidades alerta:@"Pode ter ocorrido um problema com sua Internet , Verifique e tente novamente !"];
        }
    }
}

/**
 *(Português) Ação 'btSinapse', Executa o dropDown para a escolha das opções.
 *
 *(English) Action 'btSinapse', Runs the dropdown to choose the options.
 * @author Victor Freitas (victor.alves@eticasolucoes.com.br).
 * @param id.
 * @return IBAction.
 */
- (IBAction)btSinapse:(id)sender {
    
    if(_dropDown.table == nil) {
        NSArray * arrOpcoes = @[@"Simplificada", @"Completa"];
        
        
        NSArray * arrListContent = [arrOpcoes copy];
        CGFloat dropDownListHeight = 88; //Set height of drop down list
        NSString *direction = @"down"; //Set drop down direction animation
        
        _dropDown = [[SKDropDown alloc]showDropDown:sender withHeight:&dropDownListHeight withData:arrListContent animationDirection:direction];
        _dropDown.delegate = self;
    }
    else {
        [_dropDown hideDropDown:sender];
        _dropDown = [[SKDropDown alloc]init];
    }
    
}

/**
 *(Português) Ação 'validaCamposObrigatorios', Valida os campos que são obrigatórios para disparar
 * requisição.
 *
 *(English) Action 'validaCamposObrigatorios' Validates the fields that are required to fire Request.
 * @author Victor Freitas (victor.alves@eticasolucoes.com.br).
 * @param id.
 * @return IBAction.
 */
-(BOOL) validaCamposObrigatorios {
    
    NSMutableString *camposObrigatorios = [[NSMutableString alloc] initWithString:@""];
    BOOL camposValidos = YES;
    
    if([txtTituloFilme.text length] == 0) {
        [camposObrigatorios
         appendString:@"Preencha o titulo para continuar!"];
        camposValidos = NO;
    }
    
    if([btSinapse.titleLabel.text isEqualToString:@"Selecione"]) {
        [camposObrigatorios
         appendString:@"Preencha o a Sinápse para continuar!"];
        camposValidos = NO;
    }
    
    
    
    if(!camposValidos)
        [Funcionalidades alerta:camposObrigatorios];
    
    return camposValidos;
    
}


/**
 - Este metodo verifica se existe conexao com a internet
 * @author Victor Freitas
 * @return BOOL
 */
- (BOOL) currentNetworkStatus {
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    BOOL connected;
    BOOL isConnected;
    const char *host = "www.apple.com";
    SCNetworkReachabilityRef reachability = SCNetworkReachabilityCreateWithName(NULL, host);
    SCNetworkReachabilityFlags flags;
    connected = SCNetworkReachabilityGetFlags(reachability, &flags);
    isConnected = NO;
    isConnected = connected && (flags & kSCNetworkFlagsReachable) && !(flags & kSCNetworkFlagsConnectionRequired);
    CFRelease(reachability);
    return isConnected;
}

/**
 *(Português) Ação 'parserJSON', Executa o Parser dos dados Retornados em JSON.
 * requisição.
 *
 *(English) Action 'parserJSON' Runs the Parser of Returnees data in JSON.
 * @author Victor Freitas (victor.alves@eticasolucoes.com.br).
 * @param "NSData".
 * @return NSMutableArray.
 */
-(NSMutableArray *)parserJSON:(NSData *)data{
    if(!data || [data length] == 0){
        NSLog(@"Nenhum dado encontrado");
        return nil;
    }
    
    NSMutableArray *arrayFilme = [[NSMutableArray alloc]init];
    
    //Inicializa Parser JSOn
    SBJsonParser *jsonParser = [SBJsonParser new];
    
    //Carrega NSData em um Dictionary
    NSDictionary *dictFilme = (NSDictionary *)[jsonParser objectWithData:data];
    
    @try{
        
        if([dictFilme objectForKey:@"Response"] != nil){
            
            if([[dictFilme objectForKey:@"Response"] isEqualToString:@"True"]){
                
                //O valor do filme pesquisado é lido de um NSDictionary.
                Filme *filme = [[Filme alloc]init];
                filme.resposta = [dictFilme objectForKey:@"Response"];
                filme.titulo = [dictFilme objectForKey:@"Title"];
                filme.ano = [dictFilme objectForKey:@"Year"];
                filme.classificacao = [dictFilme objectForKey:@"Rated"];
                filme.lancamento = [dictFilme objectForKey:@"Released"];
                filme.tempoDuracao = [dictFilme objectForKey:@"Runtime"];
                filme.genero = [dictFilme objectForKey:@"Genre"];
                filme.diretores = [dictFilme objectForKey:@"Director"];
                filme.escritores = [dictFilme objectForKey:@"Writer"];
                filme.atores = [dictFilme objectForKey:@"Actors"];
                filme.sinapse = [dictFilme objectForKey:@"Plot"];
                filme.linguagens = [dictFilme objectForKey:@"Language"];
                filme.pais = [dictFilme objectForKey:@"Country"];
                filme.premios = [dictFilme objectForKey:@"Awards"];
                filme.url_foto = [dictFilme objectForKey:@"Poster"];
                filme.pontuacao = [dictFilme objectForKey:@"Metascore"];
                filme.classificacaoIMDB = [dictFilme objectForKey:@"imdbRating"];
                filme.votosIMDB = [dictFilme objectForKey:@"imdbVotes"];
                filme.imdbID = [dictFilme objectForKey:@"imdbID"];
                filme.tipo = [dictFilme objectForKey:@"Type"];
                
                [arrayFilme addObject:filme];
                
            }else{
                
                NSLog(@"Filme Não Encontrado");
            }
        }else{
            
            [Funcionalidades alerta:@"Filme Não Encontrado !"];
        }
    } @catch (NSException *exception) {
        
        [Funcionalidades alerta:@"Ocorreu um erro ao pesquisar o filme desejado, Por favor tente novamente mais tarde !."];
    }
    
    
    //Retorna Filme.
    return arrayFilme;
}

#pragma mark - DropDownDelegate

/**
 * Metodo delegate que inicializa "closeDropDown" ao clicar na opção escolhida.
 * @author Victor Freitas
 */
- (void) skDropDownDelegateMethod: (SKDropDown *) sender {
    [self closeDropDown];
    
    
}

/**
 * Metodo que traz o retorno da opção escolhida.
 * @author Victor Freitas
 */
-(void)closeDropDown{
    
    NSString * elemento = _dropDown.element;
    
    if([elemento isEqualToString:@"Simplificado"]){
        postFilme.sinapse = @"short";
    }else{
        postFilme.sinapse = @"full";
    }
    
    _dropDown = nil;
    
}


#pragma  mark - HttpAsyncHelperDelegate.

//Ocorreu um erro.
-(void)requestEndWithError:(NSError *)error{
    NSLog(@"Erro ao fazer a requisição %@", [error description]);
    [Funcionalidades alerta:@"Servidor temporariamente indisponivel, por favor tente mais tarde"];
}

//Terminou a requisição
-(void)requestEndWithData:(NSData *)data{
    //Para Animação
    [progress stopAnimating];
    
    //Faz o parser
    if(data && [data length] > 0){
        Filmes = [self parserJSON:data];
        [_btPesquisar setEnabled:YES];
    }
    if(Filmes && [Filmes count] > 0){
        [self.tabelaFilmes reloadData];
        [_btPesquisar setEnabled:YES];
    }else{
        
        [_btPesquisar setEnabled:YES];
        [Funcionalidades alerta:@"Nenhum Filme encontrado." ];
    }
}



@end
