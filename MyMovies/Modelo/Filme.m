//
//  Filme.m
//  MyMovies
//
//  Created by Etica on 29/09/16.
//  Copyright © 2016 Victor Freitas. All rights reserved.
//

#import "Filme.h"

@implementation Filme
@synthesize titulo, ano, classificacao, lancamento, tempoDuracao, genero, diretores, atores, escritores, enredo, linguagens, pais, premios, classificacaoIMDB, pontuacao, votosIMDB, tipo, url_foto, sinapse, imdbID, temporadas, resposta;
@end
