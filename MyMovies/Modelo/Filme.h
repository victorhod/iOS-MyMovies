//
//  Filme.h
//  MyMovies
//
//  Created by Victor Freitas on 29/09/16.
//  Copyright © 2016 Victor Freitas. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Filme : NSObject{
    
    //Atributtos
    NSString *imdbID;
    NSString *titulo;
    NSString *ano;
    NSString *classificacao;
    NSString *lancamento;
    NSString *tempoDuracao;
    NSString *genero;
    NSMutableArray *diretores;
    NSMutableArray *escritores;
    NSMutableArray *atores;
    NSString *enredo;
    NSMutableArray *linguagens;
    NSString *pais;
    NSMutableArray *premios;
    NSString *pontuacao;
    NSString *classificacaoIMDB;
    NSString *votosIMDB;
    NSString *tipo;
    NSString *sinapse;
    NSString *temporadas;



    //URL para foto.
    NSString *url_foto;
    
    
}

//Propriedades
@property(nonnull, retain) NSString *imdbID;
@property(nonnull, retain) NSString *titulo;
@property(nonnull, retain) NSString *ano;
@property(nonnull, retain) NSString *classificacao;
@property(nonnull, retain) NSString *lancamento;
@property(nonnull, retain) NSString *tempoDuracao;
@property(nonnull, retain) NSString *genero;
@property(nonnull, retain) NSMutableArray *diretores;
@property(nonnull, retain) NSMutableArray *escritores;
@property(nonnull, retain) NSMutableArray *atores;
@property(nonnull, retain) NSString *enredo;
@property(nonnull, retain) NSMutableArray *linguagens;
@property(nonnull, retain) NSString *pais;
@property(nonnull, retain) NSMutableArray *premios;
@property(nonnull, retain) NSString *pontuacao;
@property(nonnull, retain) NSString *classificacaoIMDB;
@property(nonnull, retain) NSString *votosIMDB;
@property(nonnull, retain) NSString *tipo;
@property(nonnull, retain) NSString *url_foto;
@property(nonnull, retain) NSString *sinapse;
@property(nonnull, retain) NSString *temporadas;
@property (nonnull , retain) NSString *resposta;






@end
