//
//  PostFilme.h
//  MyMovies
//
//  Created by Etica on 30/09/16.
//  Copyright © 2016 Victor Freitas. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PostFilme : NSObject{
    
    NSString *titulo;
    NSString *ano;
    NSString *sinapse;

}

@property(nonnull, retain) NSString *titulo;
@property(nonnull, retain) NSString *ano;
@property(nonnull, retain) NSString *sinapse;

@end
